#include "../include/rotate.h"

struct image rotate( struct image const* const src ){
    struct image rotate_image = image_create(src->height,src->width);
    for (size_t row = 0; row < src->height; row++) {
        for (size_t offset = 0; offset < src->width; offset++) {
          set_pixel(&rotate_image,src->height-row-1,offset,get_pixel(src,offset,row));
        }
    }
    return rotate_image;
}
