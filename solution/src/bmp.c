#include "../include/bmp.h"
#include <stdlib.h>

#define HEADER_SIGNATURE 0x4D42
#define HEADER_PLANES 1
#define HEADER_INFO_SIZE 40
#define HEADER_BIT_COUNT 24

static uint8_t get_padding(uint64_t const width){
    	return (4 - (uint8_t)((width * sizeof(struct pixel)) % 4));
}

static enum read_status validate_header(struct bmp_header const * const header){
    if (header->bfType!=HEADER_SIGNATURE) return READ_INVALID_SIGNATURE; 
    if (header->biBitCount != HEADER_BIT_COUNT) return READ_INVALID_HEADER;
    return READ_OK;
}

static enum read_status read_header(FILE* const in, struct bmp_header* const header){
    if (!(fread(header, sizeof(struct bmp_header),1,in))) return READ_INVALID_BITS;
    if (validate_header(header)!=READ_OK) return READ_INVALID_HEADER;
    return READ_OK;
}

static enum read_status read_data(FILE* const in, struct image* const img){
    if (img==NULL || img->data == NULL) return READ_INVALID_BITS;
    uint8_t padding = get_padding(img->width);
    for (size_t i = 0; i < img->height; i++){
        if (fread(img->data + img->width*i, sizeof(struct pixel), img->width, in)!=img->width ||
            fseek(in, padding, SEEK_CUR)){
            free(img->data);
            return READ_INVALID_BITS;
        }
    }
    return READ_OK;
}

static enum write_status write_header(FILE* const out, struct bmp_header const * const header){
    if (header != NULL){
        if (fwrite(header, sizeof(struct bmp_header), 1,out)==1) return WRITE_OK;
    }
    return WRITE_ERROR;
}

static struct bmp_header create_header(uint64_t const width, uint64_t const height){
    uint64_t img_size = sizeof(struct pixel) * width *height;   
    uint64_t file_size = sizeof(struct bmp_header) + img_size;
    return (struct bmp_header) {
        .bfType = HEADER_SIGNATURE,
        .bfileSize = file_size,
        .bfReserved = 0,
        .bOffBits = sizeof(struct bmp_header),
        .biSize = HEADER_INFO_SIZE,
        .biWidth = width,
        .biHeight = height,
        .biPlanes = HEADER_PLANES,
        .biBitCount = HEADER_BIT_COUNT,
        .biCompression = 0,
        .biSizeImage = img_size,
        .biXPelsPerMeter = 0,
        .biYPelsPerMeter = 0,
        .biClrUsed = 0,
        .biClrImportant = 0
    };
}

static enum write_status write_data(FILE* const out, struct image const* const img){
    if (img==NULL || img->data == NULL) return WRITE_ERROR;
    uint8_t padding = get_padding(img->width);
    for (size_t i = 0; i < img->height; i++){
        if (fwrite(img->data + img->width*i, sizeof(struct pixel), img->width, out)!=img->width ||
            fseek(out, padding, SEEK_CUR))   return WRITE_ERROR;  
    }
    return WRITE_OK;
}

enum read_status from_bmp( FILE* const in, struct image* const img ){
    struct bmp_header header = {0};
    if (read_header(in, &header)!= READ_OK) return READ_INVALID_HEADER;
    *img = image_create(header.biWidth,header.biHeight);
    if (read_data(in, img)!= READ_OK) return READ_INVALID_BITS;
    return READ_OK;
}

enum write_status to_bmp( FILE* const out, struct image const* const img ){
    struct bmp_header header = create_header(img->width, img->height);
    if (write_header(out, &header)!= WRITE_OK || write_data(out, img)!=WRITE_OK) return WRITE_ERROR;
    return WRITE_OK;
}
