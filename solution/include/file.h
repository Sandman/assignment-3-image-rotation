#ifndef LAB_3_FILE_H
#define LAB_3_FILE_H

#include <stdio.h>

enum file_open_status {
    FL_OPEN_OK = 0,
    FL_OPEN_ERR,
    /* other status codes */
};

enum file_open_status file_open(FILE** const file, char const * const file_name, char const * const mode);

enum file_close_status {
    FL_CLOSE_OK = 0,
    FL_CLOSE_ERR
    /* other status codes */
};

enum file_close_status file_close(FILE* const file);

#endif

